---
title: "Linear Regression"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 


# Lade Trainingsdaten

Lese Daten aus Datei ein:

```{r}
MyData <- read.csv(file='./ex1data1.txt', header=FALSE, sep=',')
colnames(MyData) <- c("City Population", "Profit Foodtruck")
MyData
```

# Erste Analyse der Daten

```{r}
str(MyData)
```

```{r}
summary(MyData)
```

```{r}
m = nrow(MyData)
names(m) <- "Anzahl Trainingsdaten"
m
```
```{r}
plot(MyData)
```

```{r}
X <- as.matrix(MyData["City Population"])
Y <- as.matrix(MyData["Profit Foodtruck"])
```

# Linear Regression using lm

R bietet mit lm bereits ein lineares Modell an.

```{r}
fm <- lm(Y ~ X)
summary(fm)
```

```{r}
plot(MyData)
abline(fm)
```
# Implementierung der linearen Regression von Hand


Wir werden hier aber Gradient Descent implementieren.
Setze Konstanten für die Anzahl der Iterationen und für die Learning Rate:

```{r}
iterations <- 1500
alpha <- 0.01
X.biased <- cbind(matrix(rep(1, m)), X)
features = dim(X.biased)[2]
theta = c(0, 0)
```

Definiere die Cost-Function als Least Squared Deviation (LSD):

```{r}
cost <- function(X, Y, theta) {
  m = nrow(X)
  
  hypothesis = X %*% theta
  residual = hypothesis - Y
  squareError = residual ^ 2
  J = 1/(2*m) * colSums(squareError)
  names(J) <- "Cost"
  return(J)
}
```


Definiere Gradient Descent mit der Cost Function

```{r}
gradientDescent <- function (X, Y, theta, alpha, iterations) {
  J = numeric(iterations)
  
  for(i in 0:iterations) {
    hypothesis = X %*% theta
    residual = hypothesis - Y
    partial = t(X) %*% residual
    theta <- theta - alpha / m * partial
    
    J[i] <- cost(X, Y, theta)
  }

  unname(theta)
  
  return(list(theta=theta, J.History=J))
}
```

Errechne theta mittels gradient descent

```{r}
vals <- gradientDescent(X.biased, Y, theta, alpha, iterations)
J.theta <- as.matrix(unlist(vals["J.History"]))
plot(J.theta)
```
```{r}
plot(MyData)
koeff <- unlist(vals["theta"])
abline(koeff)
```

